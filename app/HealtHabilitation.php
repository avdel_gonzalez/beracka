<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealtHabilitation extends Model
{
    protected $table = 'healt_habilitations';
    protected $fillable = [
        'name',
        'description'
    ];
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HealtHabilitation;

class AdviceController extends Controller
{
    public function index(){
        $healt_habilitations = HealtHabilitation::orderBy('name', 'desc')->get();
        return view('auth.healt_habilitations.index')->with('healt_habilitations', $healt_habilitations);
    }

    public function create(){
        return view('auth.healt_habilitations.create');
    }

    public function store(Request $request){
        $data = [
            'name' => $request->name,
            'description' => $request->description
        ];
        $healt_habilitation = New HealtHabilitation($data);
        $healt_habilitation->save();
        flash('La norma ' . $request->name . ' ha sido creada exitosamente')->success();
        return redirect()->route('healt_habilitations.index');
    }

    public function edit($id){
        $healt_habilitation = HealtHabilitation::find($id);
        return view('auth.healt_habilitations.edit')->with('healt_habilitation', $healt_habilitation);
    }

    public function update(Request $request, $id){
        $healt_habilitation = HealtHabilitation::find($id);
        $data = [
            'name' => $request->name,
            'description' => $request->description
        ];
        $healt_habilitation->fill($data);
        $healt_habilitation->save();
        flash('La norma ' . $request->name . ' ha sido editada exitosamente')->success();
        return redirect()->route('healt_habilitations.index');
    }

    public function destroy($id){
        $healt_habilitation = HealtHabilitation::find($id);
        $healt_habilitation->delete();
        flash('La norma ['.$healt_habilitation->name.'] ha sido borrada exitosamente')->error();
        return redirect()->route('healt_habilitations.index');
    }
}

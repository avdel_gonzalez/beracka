<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class CompanyController extends Controller
{
    public function index(){
        $company = Company::find('1');
        return view('auth.company.index')->with('company', $company);
    }

    public function edit($id){
        $company = Company::find($id);
        return view('auth.company.edit')->with('company', $company);
    }

    public function update(Request $request, $id){
        $company = Company::find($id);
        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'objectives' => $request->objectives,
            'mission' => $request->mission,
            'vission' => $request->vission
        ];
        $company->fill($data);
        $company->save();
        flash('La información de la compañía ' . $request->name . ' ha sido editada exitosamente')->success();
        return redirect()->route('company.index');
    }
}

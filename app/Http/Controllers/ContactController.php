<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Mail;
use Validator;

class ContactController extends Controller
{
    public function index(){
        return view('front.contact.index');
    }

    public function contact(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'body' => 'required'
        ]);

        $name = $request->name;
        $email = $request->email;
        $subject = $request->subject;
        $body = $request->body;
        //$beracka_mail = 'davidmarquez.dev@gmail.com';
        $beracka_mail = 'arjgberacka24@gmail.com';
        
        Mail::to($beracka_mail)->send(new ContactMail($name, $email, $subject, $body));

        flash('Gracias por contactarnos, el correo fue enviado exitosamente')->success();
        return view('front.contact.index');
    }
}

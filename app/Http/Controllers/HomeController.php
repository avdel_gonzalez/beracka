<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Project;
Use App\Property;
Use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $executing_projects = Project::select('name', 'state')->where('state', '0')->get();
        $executed_projects = Project::select('name', 'state')->where('state', '1')->get();
        $rental_properties = Property::select('name', 'state')->where('state', '0')->get();
        $sale_properties = Property::select('name', 'state')->where('state', '1')->get();
        $admins = User::select('name', 'role')->where('role', 'admin')->get();
        $users = User::select('name', 'role')->where('role', 'user')->get();
        return view('auth.home')->with('executing_projects', $executing_projects)
                                ->with('executed_projects', $executed_projects)
                                ->with('rental_properties', $rental_properties)
                                ->with('sale_properties', $sale_properties)
                                ->with('admins', $admins)
                                ->with('users', $users);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\ImageMember;

class MembersController extends Controller
{
    public function index(){
        $members = Member::orderBy('name', 'desc')->get();
        $members->each(function($members){
            $members->memberImages;
        });
        return view('auth.members.index')->with('members', $members);
    }

    public function create(){
        return view('auth.members.create');
    }

    public function store(Request $request){
        $data = [
            'name' => $request->name,
            'description' => $request->description
        ];
        $member = new Member($data);
        $member->save();
        $allowedfileExtension=['jpg','png'];
        $files = $request->member_images;
        foreach($files as $i => $file){
            $file_name = $request->name . '-' . $i . $file->getClientOriginalExtension();
            $path = public_path().'/img/members/';
            $file->move($path, $file_name);
            $image_data = [
                'name' => $file_name,
                'member_id' => $member->id,
            ];
            $member_image = new ImageMember($image_data);
            $member_image->save();
        }
        flash('El miembro ' . $request->name . ' ha sido creado exitosamente')->success();
        return redirect()->route('members.index');
    }

    public function edit($id){
        $member = Member::find($id);
        $member->memberImages;
        return view('auth.members.edit')->with('member', $member);
    }

    public function update(Request $request, $id){
        $data = [
            'name' => $request->name,
            'description' => $request->description,
        ];
        $member = Member::find($id);
        $member->fill($data);
        $member->save();
        $allowedfileExtension=['jpg','png'];
        $files = $request->member_images;
        if($request->member_images){
            foreach($files as $i => $file){
                $file_name = $request->name . '-' . $i . $file->getClientOriginalExtension();
                $path = public_path().'/img/members/';
                $file->move($path, $file_name);
                $image_data = [
                    'name' => $file_name,
                    'member_id' => $member->id,
                ];
                $member_image = new ImageMember($image_data);
                $member_image->save();
            }
        }        
        flash('El miembro ' . $request->name . ' ha sido editado exitosamente')->success();
        return redirect()->route('members.index');
    }

    public function imageDelete($id){
        $image_member = ImageMember::find($id);
        $image_member->delete();
        flash('Se eliminó la imagen exitosamente')->error();
        return redirect()->back();
    }

    public function destroy($id){
        $member = Member::find($id);
        $member->delete();
        flash('El proyecto ' . $member->name . ' ha sido eliminado exitosamente')->error();
        return redirect()->route('members.index');
    }
}

<?php

namespace App\Http\Controllers;
Use App\Company;
Use App\Member;
Use App\ImageMember;
Use App\Project;
Use App\ProjectImage;
Use App\Property;
Use App\PropertyImage;
use App\HealtHabilitation;
use App\Video;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        $company = Company::find('1');
        $members = Member::orderBy('name', 'desc')->get();
        $members->each(function($members){
            $members->memberImages;
        });
        //dd($company);
        return view('front.index')->with('company', $company)
                              ->with('members', $members);
    }

    public function ejecutingProjects(){
        $projects = Project::where('state', '0')->orderBy('created_at', 'desc')->get();
        $projects->each(function($projects){
            $projects->projectImages;
        });
        return view('front.construction.ejecuting')->with('projects', $projects);
    }

    public function ejecutedProjects(){
        $projects = Project::where('state', '1')->orderBy('created_at', 'desc')->get();
        $projects->each(function($projects){
            $projects->projectImages;
        });
        return view('front.construction.ejecuted')->with('projects', $projects);
    }

    public function rentalProperties(){
        $properties = Property::where('state', '0')->orderBy('created_at', 'desc')->get();
        $properties->each(function($properties){
            $properties->PropertyImages;
        });
        return view('front.real-state.rental')->with('properties', $properties);
    }

    public function saleProperties(){
        $properties = Property::where('state', '1')->orderBy('created_at', 'desc')->get();
        $properties->each(function($properties){
            $properties->PropertyImages;
        });
        return view('front.real-state.sale')->with('properties', $properties);
    }

    public function adviceHealt(){
        $healt_habilitations = HealtHabilitation::orderBy('name', 'desc')->get();
        return view('front.advice.index')->with('healt_habilitations', $healt_habilitations);
    }

    public function videos(){
        $videos = Video::orderBy('name', 'desc')->get();
        return view('front.videos.index')->with('videos', $videos);
    }
}

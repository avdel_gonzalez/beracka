<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\ProjectImage;

class ProjectsController extends Controller
{
    public function index(){
        $projects = Project::orderBy('name', 'desc')->get();
        $projects->each(function($projects){
            $projects->projectImages;
        });
        return view('auth.projects.index')->with('projects', $projects);
    }

    public function create(){
        return view('auth.projects.create');
    }

    public function store(Request $request){
        $data = [
            'name' => $request->name,
            'rooms' => $request->rooms,
            'size' => $request->size,
            'annual_taxes' => $request->annual_taxes,
            'price' => $request->price,
            'administration' => $request->administration,
            'ubication' => $request->ubication,
            'state' => $request->state
        ];
        $project = new Project($data);
        $project->save();
        $allowedfileExtension=['jpg','png'];
        $files = $request->project_images;
        foreach($files as $i => $file){
            $file_name = $request->name . '-' . $i . $file->getClientOriginalExtension();
            $path = public_path().'/img/projects/';
            $file->move($path, $file_name);
            $image_data = [
                'name' => $file_name,
                'project_id' => $project->id,
            ];
            $project_image = new ProjectImage($image_data);
            $project_image->save();
        }
        flash('El proyecto ' . $request->name . ' ha sido creado exitosamente')->success();
        return redirect()->route('projects.index');
    }

    public function edit($id){
        $project = Project::find($id);
        $project->projectImages;
        return view('auth.projects.edit')->with('project', $project);
    }

    public function update(Request $request, $id){
        $data = [
            'name' => $request->name,
            'rooms' => $request->rooms,
            'size' => $request->size,
            'annual_taxes' => $request->annual_taxes,
            'price' => $request->price,
            'administration' => $request->administration,
            'ubication' => $request->ubication,
            'state' => $request->state
        ];
        $project = Project::find($id);
        $project->fill($data);
        $project->save();
        $allowedfileExtension=['jpg','png'];
        $files = $request->project_images;
        if($request->project_images){
            foreach($files as $i => $file){
                $file_name = $request->name . '-' . $i . $file->getClientOriginalExtension();
                $path = public_path().'/img/projects/';
                $file->move($path, $file_name);
                $image_data = [
                    'name' => $file_name,
                    'project_id' => $project->id,
                ];
                $project_image = new ProjectImage($image_data);
                $project_image->save();
            }
        }        
        flash('El proyecto ' . $request->name . ' ha sido editado exitosamente')->success();
        return redirect()->route('projects.index');
    }

    public function imageDelete($id){
        $image_project = ProjectImage::find($id);
        $image_project->delete();
        flash('Se eliminó la imagen exitosamente')->error();
        return redirect()->back();
    }

    public function destroy($id){
        $project = Project::find($id);
        $project->delete();
        flash('El proyecto ' . $project->name . ' ha sido eliminado exitosamente')->error();
        return redirect()->route('projects.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\PropertyImage;

class PropertiesController extends Controller
{
    public function index(){
        $properties = Property::orderBy('name', 'desc')->get();
        $properties->each(function($properties){
            $properties->PropertyImages;
        });
        return view('auth.properties.index')->with('properties', $properties);
    }

    public function create(){
        return view('auth.properties.create');
    }

    public function store(Request $request){
        $data = [
            'name' => $request->name,
            'rooms' => $request->rooms,
            'size' => $request->size,
            'annual_taxes' => $request->annual_taxes,
            'price' => $request->price,
            'administration' => $request->administration,
            'ubication' => $request->ubication,
            'state' => $request->state
        ];
        $property = new Property($data);
        $property->save();
        $allowedfileExtension=['jpg','png'];
        $files = $request->property_images;
        foreach($files as $i => $file){
            $file_name = $request->name . '-' . $i . $file->getClientOriginalExtension();
            $path = public_path().'/img/properties/';
            $file->move($path, $file_name);
            $image_data = [
                'name' => $file_name,
                'property_id' => $property->id,
            ];
            $property_image = new PropertyImage($image_data);
            $property_image->save();
        }
        flash('La propiedad ' . $request->name . ' ha sido creado exitosamente')->success();
        return redirect()->route('properties.index');
    }

    public function edit($id){
        $property = Property::find($id);
        $property->propertyImages;
        return view('auth.properties.edit')->with('property', $property);
    }

    public function update(Request $request, $id){
        $data = [
            'name' => $request->name,
            'rooms' => $request->rooms,
            'size' => $request->size,
            'annual_taxes' => $request->annual_taxes,
            'price' => $request->price,
            'administration' => $request->administration,
            'ubication' => $request->ubication,
            'state' => $request->state
        ];
        $property = Property::find($id);
        $property->fill($data);
        $property->save();
        $allowedfileExtension=['jpg','png'];
        $files = $request->property_images;
        if($request->property_images){
            foreach($files as $i => $file){
                $file_name = $request->name . '-' . $i . $file->getClientOriginalExtension();
                $path = public_path().'/img/properties/';
                $file->move($path, $file_name);
                $image_data = [
                    'name' => $file_name,
                    'property_id' => $property->id,
                ];
                $property_image = new PropertyImage($image_data);
                $property_image->save();
            }
        }        
        flash('La propiedad ' . $request->name . ' ha sido editado exitosamente')->success();
        return redirect()->route('properties.index');
    }

    public function imageDelete($id){
        $image_property = PropertyImage::find($id);
        $image_property->delete();
        flash('Se eliminó la imagen exitosamente')->error();
        return redirect()->back();
    }

    public function destroy($id){
        $property = Property::find($id);
        $property->delete();
        flash('El proyecto ' . $property->name . ' ha sido eliminado exitosamente')->error();
        return redirect()->route('properties.index');
    }
}

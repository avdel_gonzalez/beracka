<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Laracasts\Flash\Flash;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::orderBy('name', 'asc')->get();
        return view('auth.users.index')->with('users', $users);
    }
    
    public function create(){
        return view('auth.users.create');
    }

    public function store(Request $request){
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $request->role
        ];
        $user = New User($data);
        $user->save();
        flash('El usuario ' . $request->name . ' ha sido creado exitosamente')->success();
        return redirect()->route('users.index');
    }

    public function edit($id){
        $user = User::find($id);
        return view('auth.users.edit')->with('user', $user);
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        if($request->password != null){
            $request->password = bcrypt($request->password);
        } else {
            $request->password = $user->password;
        }
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'role' => $request->role
        ];
        $user->fill($data);
        $user->save();
        flash('El usuario ' . $request->name . ' ha sido editado exitosamente')->success();
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        flash('El usuario ['.$user->name.'] ha sido borrado exitosamente')->error();
        return redirect()->route('users.index');
    }
}

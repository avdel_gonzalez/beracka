<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideosController extends Controller
{
    public function index(){
        $videos = Video::orderBy('name', 'desc')->get();
        return view('auth.videos.index')->with('videos', $videos);
    }

    public function create(){
        return view('auth.videos.create');
    }

    public function store(Request $request){
        $data = [
            'name' => $request->name,
            'description' => $request->description
        ];
        $video = New Video($data);
        $video->save();
        flash('El video ' . $request->name . ' ha sido creado exitosamente')->success();
        return redirect()->route('videos.index');
    }

    public function edit($id){
        $video = Video::find($id);
        return view('auth.videos.edit')->with('video', $video);
    }

    public function update(Request $request, $id){
        $video = Video::find($id);
        $data = [
            'name' => $request->name,
            'description' => $request->description
        ];
        $video->fill($data);
        $video->save();
        flash('El video ' . $request->name . ' ha sido editado exitosamente')->success();
        return redirect()->route('videos.index');
    }

    public function destroy($id){
        $video = Video::find($id);
        $video->delete();
        flash('El video ['.$video->name.'] ha sido borrado exitosamente')->error();
        return redirect()->route('videos.index');
    }
}

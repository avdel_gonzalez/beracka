<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageMember extends Model
{
    protected $table = 'members_images';

    protected $fillable = [
        'name',
        'member_id'
    ];

    public function member(){
        return $this->belogsTo('App\Member');
    }
}

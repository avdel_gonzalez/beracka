<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'members';

    protected $fillable = [
        'name',
        'description'
    ];

    public function memberImages(){
        return $this->hasMany('App\ImageMember');
    }
}

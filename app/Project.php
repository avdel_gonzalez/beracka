<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    protected $fillable = [
        'name',
        'rooms',
        'size',
        'annual_taxes',
        'price',
        'administration',
        'ubication',
        'state'
    ];

    public function projectImages(){
        return $this->hasMany('App\ProjectImage');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectImage extends Model
{
    protected $table = 'projects_images';

    protected $fillable = [
        'name',
        'project_id'
    ];

    public function project(){
        return $this->belongsTo('App\Project');
    }
}

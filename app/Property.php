<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'properties';

    protected $fillable = [
        'name',
        'rooms',
        'size',
        'annual_taxes',
        'price',
        'administration',
        'ubication',
        'state'
    ];

    public function propertyImages(){
        return $this->hasMany('App\PropertyImage');
    }
}

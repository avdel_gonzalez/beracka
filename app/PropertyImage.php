<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyImage extends Model
{
    protected $table = 'properties_images';

    protected $fillable = [
        'name',
        'property_id'
    ];

    public function property(){
        return $this->belongsTo('App\Property');
    }
}

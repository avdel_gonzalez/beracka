<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'David Felipe Márquez González',
            'email' => 'davidmarquez.dev@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 'admin'
        ]);

        DB::table('company_info')->insert([
            'name' => 'Beracka',
            'description' => 'Somos una sociedad comercial por acciones simplificada, de nacionalidad colombiana, El domicilio principal de la sociedad por el momento es : Ciudad de Bogota  ,Av Jimenez (calle 13 # 5-30) oficina 307, La sociedad puede crear sucursales , agencias y establecimientos por decisión de la asamblea general de accionistas .',
            'objectives' => 'Ofrecer y Ejecutar las actividades relacionadas o inherentes al campo de la Arquitectura ,Urbanismo,Planeamiento ,,etc , con un enfoque ambiental en: Construcción: Obras civiles,estudios de suelos, calculo y diseño estructural ,Adecuaciones, Remo delaciones ,Etc. Inmobiliaria: Compra y venta de inmuebles , Avaluos , Administración de inmuebles , Etc. Asesorías en Norma: Asesorias en normas urbaisticas y de construcción, consultorias e interventorias de obras , Asesorias en Normas de habilitación en salud,Etc',
            'mission' => 'La Construtora e Inmobiliria Beracka . ARJG SAS empresa modelo en la construcción de Infraestructura hospitalaria, asesoría en normas de Salud, proyectos de obras civiles, con profesionales idoneos altamente calificados con base en los principios de prestación de servios bajo los lineamientos de los estándares establecidos por la normatividad vigente en salud, sistema integral de gestión en calidad, seguridad, salud ocupacional y el medio ambiente. Satisfacción plena de los clientes en relación a nuestros servicios accediendo accediendo a las expectativas  y bienestar del personal y de las familias y la sociedad en general.',
            'vission' => 'Ser en el año 2030 una empresa solida y reconocida en toda Colombia por la prestación de servicios integrales en las diferentes áreas de la construcción, normas de salud del sector inmobiliario dando solución a la comunidad  con estándares de calidad y seguridad, así como respetando e integrando el ambiente.'
        ]);
    }
}

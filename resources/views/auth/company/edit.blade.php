@extends('auth.layouts.app')

@section('content')
    @if(Auth::user()->role == 'admin')
        <h2>
            Editar Compañía: {{ $company->name }}
        </h2>
        <div class="container">
            {!! Form::open(['route' => ['company.update', $company], 'method' => 'put']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Nombre') !!}
                </div>
                <div class="form-group">
                    {!! Form::text('name', $company->name, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required' => 'true']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Descripción') !!}
                </div>
                <div class="form-group">
                    {!! Form::text('description', $company->description, ['class' => 'form-control', 'placeholder' => 'Descripción', 'required' => 'true']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('objectives', 'Objetivos') !!}
                </div>
                <div class="form-group">
                    {!! Form::text('objectives', $company->objectives, ['class' => 'form-control', 'placeholder' => 'Objetivos', 'required' => 'true']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('mission', 'Misión') !!}
                </div>
                <div class="form-group">
                    {!! Form::text('mission', $company->mission, ['class' => 'form-control', 'placeholder' => 'Misión', 'required' => 'true']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('vission', 'Visión') !!}
                </div>
                <div class="form-group">
                    {!! Form::text('vission', $company->vission, ['class' => 'form-control', 'placeholder' => 'Visión', 'required' => 'true']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    @else
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Oops!!</h4>
                <p class="card-category">Usted no tiene permiso para acceder a este módulo</p>
            </div>
        </div>
    @endif
@endsection
@extends('auth.layouts.app')

@section('content')
    @if(Auth::user()->role == 'admin')
        <a href="{{ route('company.edit', $company->id) }}" class="btn btn-warning">
            Editar información de Compañía
        </a>
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Compañía: {{ $company->name }}</h4>
                <p class="card-category">Información de Compañía</p>
            </div>
            <div class="card-body">
                <strong>Nombre: </strong> {{ $company->name }}<br />
                <strong>Descripción: </strong> {{ $company->description }}<br />
                <strong>Objetivos: </strong> {{ $company->objectives }}<br />
                <strong>Misión: </strong> {{ $company->mission }}<br />
                <strong>Visión: </strong> {{ $company->vission }}<br />
            </div>
        </div>
    @else
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Oops!!</h4>
                <p class="card-category">Usted no tiene permiso para acceder a este módulo</p>
            </div>
        </div>
    @endif
@endsection
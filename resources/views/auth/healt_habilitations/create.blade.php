@extends('auth.layouts.app')

@section('content')
<h2>
    Agregar una nueva norma
</h2>
<div class="container">
    {!! Form::open(['action' => 'AdviceController@store', 'method' => 'post']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nombre de la norma') !!}
        </div>
        <div class="form-group">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre de la norma', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Descripción') !!}
        </div>
        <div class="form-group">
            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Url de la norma', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection
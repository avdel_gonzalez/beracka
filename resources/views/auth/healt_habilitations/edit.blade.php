@extends('auth.layouts.app')

@section('content')
<h2>
    Editar propiedad: {{ $healt_habilitation->name }}
</h2>
<div class="container">
    {!! Form::open(['action' => ['AdviceController@update', $healt_habilitation], 'method' => 'put']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nombre de la norma') !!}
        </div>
        <div class="form-group">
            {!! Form::text('name', $healt_habilitation->name, ['class' => 'form-control', 'placeholder' => 'Nombre de la norma', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Descripción') !!}
        </div>
        <div class="form-group">
            {!! Form::text('description', $healt_habilitation->description, ['class' => 'form-control', 'placeholder' => 'Url de la norma', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection
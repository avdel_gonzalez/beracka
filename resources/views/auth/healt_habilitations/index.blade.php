@extends('auth.layouts.app')

@section('content')
        <a href="{{ route('healt_habilitations.create') }}" class="btn btn-warning">
            Agregar una norma
        </a>
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Normas de habilitación en salud</h4>
                <p class="card-category">Normas registradas en el sistema</p>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>
                                Id
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Url
                            </th>
                            <th>
                                Acciones
                            </th>
                        </thead>
                        <tbody>
                            @foreach($healt_habilitations as $advisory)
                                <tr>
                                    <td>
                                        {{ $advisory->id }}
                                    </td>
                                    <td>
                                        {{ $advisory->name }}
                                    </td>
                                    <td>
                                        {{ $advisory->description }}
                                    </td>
                                    <td>
                                        <a href="{{ route('healt_habilitations.edit', $advisory->id) }}">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                        <a href="{{ route('healt_habilitations.destroy', $advisory->id) }}" onclick="return confirm('¿Seguro que desea eliminar este usuario?')">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection
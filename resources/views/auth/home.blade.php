@extends('auth.layouts.app')

@section('content')
    <div class="card ">
        <div class="card-header card-header-warning">
                <h4 class="card-title mt-0"><strong>Dashboard</strong></h4>
                <p class="card-category">
                    <strong>
                    BIENVENIDO A BERACKA<br />
                    Este es el panel de administrador. En esta sección usted encontrará un resumen
                    en gráficos acerca del comportamiento de su compañía.
                    </strong>
                </p>
            </div>
        <div class="card-body">
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
            <script type="text/javascript">
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {

                    var data = google.visualization.arrayToDataTable([
                        ['Proyectos', 'Porcentaje'],
                            ['En ejecución',     {{ count($executing_projects) }}],
                            ['Ejecutados',      {{ count($executed_projects) }}],
                    ]);

                    var options = {
                    title: 'Proyectos'
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('projects'));

                    chart.draw(data, options);
                }
            </script>
            <script type="text/javascript">
                google.charts.load("current", {packages:["corechart"]});
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                    ['Propiedades', 'Porcentaje'],
                        ['En Arriendo', {{ count($rental_properties) }}],
                        ['En Venta', {{ count($sale_properties) }}],
                    ]);

                    var options = {
                    title: 'Propiedades',
                    pieHole: 0.4,
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('properties'));
                    chart.draw(data, options);
                }
            </script>
            <script type="text/javascript">
            google.charts.load("current", {packages:["corechart"]});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                ['Usuarios', 'Roles'],
                ['Administradores', {{ count($admins) }}],
                ['Usuarios', {{ count($users) }}],
                ]);

                var options = {
                title: 'Tipos de Usuarios',
                pieHole: 0.4,
                };

                var chart = new google.visualization.PieChart(document.getElementById('users'));
                chart.draw(data, options);
            }
            </script>
            <div class="row">
                <div class="col-md-6">
                    <div id="projects" style="width: 100%; height: 500px;"></div>
                </div>
                <div class="col-md-6">
                    <div id="properties" style="width: 100%; height: 500px;"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div id="users" style="width: 100%; height: 500px;"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('img/sidebar-1.jpg') }}">
    <div class="logo">
        <a href="#" class="simple-text logo-normal">
            beracka
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('projects.index') }}">
                    <i class="material-icons">library_books</i>
                    <p>Proyectos</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('properties.index') }}">
                    <i class="material-icons">library_books</i>
                    <p>Propiedades</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('healt_habilitations.index') }}">
                    <i class="material-icons">flag</i>
                    <p>Habilitación en salud</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('videos.index') }}">
                    <i class="material-icons">video_library</i>
                    <p>Tutoriales</p>
                </a>
            </li>
            @if(Auth::user()->role == 'admin')
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('users.index') }}">
                        <i class="material-icons">person</i>
                        <p>Usuarios</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('company.index') }}">
                        <i class="material-icons">content_paste</i>
                        <p>Compañía</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('members.index') }}">
                        <i class="material-icons">person</i>
                        <p>Equipo de trabajo</p>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
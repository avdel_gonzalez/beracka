@extends('auth.layouts.app')

@section('content')
<h2>
    Agregar un nuevo miembro
</h2>
<div class="container">
    {!! Form::open(['action' => 'MembersController@store', 'method' => 'post', 'files' => 'true']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nombre del miembro') !!}
        </div>
        <div class="form-group">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre del miembro', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Descripción') !!}
        </div>
        <div class="form-group">
            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Cargo u oficio del miembro', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('member_images') !!}
        </div>
        <div class="form-group">
            <input type="file" class="form-control" name="member_images[]" multiple />
        </div>
        <div class="form-group">
            {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection
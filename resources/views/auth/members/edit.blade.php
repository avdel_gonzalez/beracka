@extends('auth.layouts.app')

@section('content')
<h2>
    Editar propiedad: {{ $member->name }}
</h2>
<div class="container">
    {!! Form::open(['action' => ['MembersController@update', $member], 'method' => 'put', 'files' => 'true']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nombre del miembro') !!}
        </div>
        <div class="form-group">
            {!! Form::text('name', $member->name, ['class' => 'form-control', 'placeholder' => 'Nombre del miembro', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Descripciónn') !!}
        </div>
        <div class="form-group">
            {!! Form::text('description', $member->description, ['class' => 'form-control', 'placeholder' => 'Cargo u oficio del miembro', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('member_images') !!}
        </div>
        <div class="form-group">
            <div class="row">
                @foreach($member->memberImages as $i => $image)
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <img style="width:100%; max-height: 320px" src="{{ asset('img/members/' . $image->name) }}">
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('imagemember.destroy', $image->id) }}" onclick="return confirm('¿Seguro que desea eliminar esta imagen?, Esta decisión no tiene reversa')">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="form-group">
            <input type="file" class="form-control" name="member_images[]" multiple />
        </div>
        <div class="form-group">
            {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection
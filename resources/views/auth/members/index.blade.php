@extends('auth.layouts.app')
<link href="{{ asset('css/admin/carousel.css') }}" rel="stylesheet">
@section('content')
    <a href="{{ route('members.create') }}" class="btn btn-warning">
        Agregar un nuevo miembro
    </a>
    <div class="row">
        @foreach($members as $member)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header text-center">
                        <strong>{{ $member->name }}</strong>
                    </div>
                    <div class="card-body">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            @foreach($member->memberImages as $i => $image)
                                <div
                                    @if($i == 0) 
                                        class="item active"
                                    @else
                                        class="item"
                                    @endif
                                >
                                    <img style="width:100%; max-height: 320px" src="{{ asset('img/members/'.$image->name) }}">
                                </div>
                            @endforeach
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                            <span class="sr-only">Next</span>
                        </a>
                        </div>
                    </div>
                    <div class="card-body text-center">
                        <strong>Descripción: </strong>{{ $member->description }}<br />
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('members.edit', $member->id) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Editar
                        </a> | 
                        <a href="{{ route('members.destroy', $member->id) }}" onclick="return confirm('¿Seguro que desea eliminar este miembro?')">
                            <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('.carousel').carousel()
    </script>
@endsection
@extends('auth.layouts.app')

@section('content')
<h2>
    Agregar un nuevo Proyecto
</h2>
<div class="container">
    {!! Form::open(['action' => 'ProjectsController@store', 'method' => 'post', 'files' => 'true']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nombre del Proyecto') !!}
        </div>
        <div class="form-group">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre del Proyecto', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('rooms', 'Habitaciones') !!}
        </div>
        <div class="form-group">
            {!! Form::text('rooms', null, ['class' => 'form-control', 'placeholder' => 'N° de Habitaciones', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('size', 'Tamaño') !!}
        </div>
        <div class="form-group">
            {!! Form::text('size', null, ['class' => 'form-control', 'placeholder' => 'Tamaño del proyecto', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('annual_taxes', 'Impuestos anuales') !!}
        </div>
        <div class="form-group">
            {!! Form::text('annual_taxes', null, ['class' => 'form-control', 'placeholder' => 'Impuestos (digite un valor con signo $ y . ej. $50.000.000)', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('price', 'Precio') !!}
        </div>
        <div class="form-group">
            {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Precio (digite un valor con signo $ y . ej. $50.000.000)', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('administration', 'Administración') !!}
        </div>
        <div class="form-group">
            {!! Form::text('administration', null, ['class' => 'form-control', 'placeholder' => 'Administración', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('ubication', 'Ubicación') !!}
        </div>
        <div class="form-group">
            {!! Form::text('ubication', null, ['class' => 'form-control', 'placeholder' => 'Digite aquí solamente la Ciudad / País en donde se encuentra el proyecto', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('state', 'Estado') !!}
        </div>
        <div class="form-group">
            {!! Form::select('state',['0' => 'En ejecución', '1' => 'Ejecutado'], null, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('project_images') !!}
        </div>
        <div class="form-group">
            <input type="file" class="form-control" name="project_images[]" multiple />
        </div>
        <div class="form-group">
            {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection
@extends('auth.layouts.app')

@section('content')
<h2>
    Editar proyecto: {{ $project->name }}
</h2>
<div class="container">
    {!! Form::open(['action' => ['ProjectsController@update', $project], 'method' => 'put', 'files' => 'true']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nombre del Proyecto') !!}
        </div>
        <div class="form-group">
            {!! Form::text('name', $project->name, ['class' => 'form-control', 'placeholder' => 'Nombre del Proyecto', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('rooms', 'Habitaciones') !!}
        </div>
        <div class="form-group">
            {!! Form::text('rooms', $project->rooms, ['class' => 'form-control', 'placeholder' => 'N° de Habitaciones', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('size', 'Tamaño') !!}
        </div>
        <div class="form-group">
            {!! Form::text('size', $project->size, ['class' => 'form-control', 'placeholder' => 'Tamaño del proyecto', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('annual_taxes', 'Impuestos anuales') !!}
        </div>
        <div class="form-group">
            {!! Form::text('annual_taxes', $project->annual_taxes, ['class' => 'form-control', 'placeholder' => 'Impuestos (digite un valor con signo $ y . ej. $50.000.000)', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('price', 'Precio') !!}
        </div>
        <div class="form-group">
            {!! Form::text('price', $project->price, ['class' => 'form-control', 'placeholder' => 'Precio (digite un valor con signo $ y . ej. $50.000.000)', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('administration', 'Administración') !!}
        </div>
        <div class="form-group">
            {!! Form::text('administration', $project->administration, ['class' => 'form-control', 'placeholder' => 'Administración', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('ubication', 'Ubicación') !!}
        </div>
        <div class="form-group">
            {!! Form::text('ubication', $project->ubication, ['class' => 'form-control', 'placeholder' => 'Digite aquí solamente la Ciudad / País en donde se encuentra el proyecto', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('state', 'Estado') !!}
        </div>
        <div class="form-group">
            {!! Form::select('state',['0' => 'En ejecución', '1' => 'Ejecutado'], $project->state, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('project_images') !!}
        </div>
        <div class="form-group">
            <div class="row">
                @foreach($project->projectImages as $i => $image)
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <img style="width:100%; max-height: 320px" src="{{ asset('img/projects/' . $image->name) }}">
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('imageproject.destroy', $image->id) }}" onclick="return confirm('¿Seguro que desea eliminar esta imagen?, Esta decisión no tiene reversa')">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="form-group">
            <input type="file" class="form-control" name="project_images[]" multiple />
        </div>
        <div class="form-group">
            {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection
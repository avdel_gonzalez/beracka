@extends('auth.layouts.app')
<link href="{{ asset('css/admin/carousel.css') }}" rel="stylesheet">
@section('content')
    <a href="{{ route('projects.create') }}" class="btn btn-warning">
        Agregar un nuevo proyecto
    </a>
    <div class="row">
        @foreach($projects as $project)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        {{ $project->name }}
                    </div>
                    <div class="card-body">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            @foreach($project->projectImages as $i => $image)
                                <div
                                    @if($i == 0) 
                                        class="item active"
                                    @else
                                        class="item"
                                    @endif
                                >
                                    <img style="width:100%; max-height: 320px" src="{{ asset('img/projects/'.$image->name) }}">
                                </div>
                            @endforeach
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                            <span class="sr-only">Next</span>
                        </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <strong>Habitaciones: </strong>{{ $project->rooms }}<br />
                        <strong>Tamaño: </strong>{{ $project->size }}<br />
                        <strong>Impuestos anuales: </strong>{{ $project->annual_taxes }}<br />
                        <strong>Precio: </strong>{{ $project->price }}<br />
                        <strong>Administración: </strong>{{ $project->administration }}<br />
                        <strong>Ubicación: </strong>{{ $project->ubication }}<br />
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('projects.edit', $project->id) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Editar
                        </a> | 
                        <a href="{{ route('projects.destroy', $project->id) }}" onclick="return confirm('¿Seguro que desea eliminar este proyecto?')">
                            <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('.carousel').carousel()
    </script>
@endsection
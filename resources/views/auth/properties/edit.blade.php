@extends('auth.layouts.app')

@section('content')
<h2>
    Editar propiedad: {{ $property->name }}
</h2>
<div class="container">
    {!! Form::open(['action' => ['PropertiesController@update', $property], 'method' => 'put', 'files' => 'true']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nombre de la propiedad') !!}
        </div>
        <div class="form-group">
            {!! Form::text('name', $property->name, ['class' => 'form-control', 'placeholder' => 'Nombre de la propiedad', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('rooms', 'Habitaciones') !!}
        </div>
        <div class="form-group">
            {!! Form::text('rooms', $property->rooms, ['class' => 'form-control', 'placeholder' => 'N° de Habitaciones', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('size', 'Tamaño') !!}
        </div>
        <div class="form-group">
            {!! Form::text('size', $property->size, ['class' => 'form-control', 'placeholder' => 'Tamaño de la propiedad', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('annual_taxes', 'Impuestos anuales') !!}
        </div>
        <div class="form-group">
            {!! Form::text('annual_taxes', $property->annual_taxes, ['class' => 'form-control', 'placeholder' => 'Impuestos (digite un valor con signo $ y . ej. $50.000.000)', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('price', 'Precio') !!}
        </div>
        <div class="form-group">
            {!! Form::text('price', $property->price, ['class' => 'form-control', 'placeholder' => 'Precio (digite un valor con signo $ y . ej. $50.000.000)', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('administration', 'Administración') !!}
        </div>
        <div class="form-group">
            {!! Form::text('administration', $property->administration, ['class' => 'form-control', 'placeholder' => 'Administración', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('ubication', 'Ubicación') !!}
        </div>
        <div class="form-group">
            {!! Form::text('ubication', $property->ubication, ['class' => 'form-control', 'placeholder' => 'Digite aquí solamente la Ciudad / País en donde se encuentra el proyecto', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('state', 'Estado') !!}
        </div>
        <div class="form-group">
            {!! Form::select('state',['0' => 'Arriendo', '1' => 'Venta'], $property->state, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('property_images') !!}
        </div>
        <div class="form-group">
            <div class="row">
                @foreach($property->propertyImages as $i => $image)
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <img style="width:100%; max-height: 320px" src="{{ asset('img/properties/' . $image->name) }}">
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('imageproperty.destroy', $image->id) }}" onclick="return confirm('¿Seguro que desea eliminar esta imagen?, Esta decisión no tiene reversa')">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="form-group">
            <input type="file" class="form-control" name="property_images[]" multiple />
        </div>
        <div class="form-group">
            {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection
@extends('auth.layouts.app')
<link href="{{ asset('css/admin/carousel.css') }}" rel="stylesheet">
@section('content')
    <a href="{{ route('properties.create') }}" class="btn btn-warning">
        Agregar una nueva propiedad
    </a>
    <div class="row">
        @foreach($properties as $property)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        {{ $property->name }}
                    </div>
                    <div class="card-body">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            @foreach($property->propertyImages as $i => $image)
                                <div
                                    @if($i == 0) 
                                        class="item active"
                                    @else
                                        class="item"
                                    @endif
                                >
                                    <img style="width:100%; max-height: 320px" src="{{ asset('img/properties/'.$image->name) }}">
                                </div>
                            @endforeach
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                            <span class="sr-only">Next</span>
                        </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <strong>Habitaciones: </strong>{{ $property->rooms }}<br />
                        <strong>Tamaño: </strong>{{ $property->size }}<br />
                        <strong>Impuestos anuales: </strong>{{ $property->annual_taxes }}<br />
                        <strong>Precio: </strong>{{ $property->price }}<br />
                        <strong>Administración: </strong>{{ $property->administration }}<br />
                        <strong>Ubicación: </strong>{{ $property->ubication }}<br />
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('properties.edit', $property->id) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Editar
                        </a> | 
                        <a href="{{ route('properties.destroy', $property->id) }}" onclick="return confirm('¿Seguro que desea eliminar esta propiedad?')">
                            <i class="fa fa-trash" aria-hidden="true"></i> Eliminar
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('.carousel').carousel()
    </script>
@endsection
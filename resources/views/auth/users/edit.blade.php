@extends('auth.layouts.app')

@section('content')
    @if(Auth::user()->role == 'admin')
        <h2>
            Editar usuario: {{ $user->name }}
        </h2>
        <div class="container">
            {!! Form::open(['route' => ['users.update', $user], 'method' => 'put']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Nombre completo') !!}
                </div>
                <div class="form-group">
                    {!! Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'Nombre Completo', 'required' => 'true']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Correo electrónico') !!}
                </div>
                <div class="form-group">
                    {!! Form::text('email', $user->email, ['class' => 'form-control', 'placeholder' => 'ejemplo@ejemplo.com', 'required' => 'true']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Contraseña') !!}
                </div>
                <div class="form-group">
                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '********']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('role', 'Tipo de usuario') !!}
                </div>
                <div class="form-group">
                    {!! Form::select('role',['admin' => 'Administrador', 'user' => 'Usuario'], $user->role, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción', 'required' => 'true']) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    @else
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Oops!!</h4>
                <p class="card-category">Usted no tiene permiso para acceder a este módulo</p>
            </div>
        </div>
    @endif
@endsection
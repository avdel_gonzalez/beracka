@extends('auth.layouts.app')

@section('content')
    @if(Auth::user()->role == 'admin')
        <a href="{{ route('users.create') }}" class="btn btn-warning">
            Agregar un usuario
        </a>
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Usuarios</h4>
                <p class="card-category">Usuarios registrados en el sistema</p>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>
                                Id
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Rol
                            </th>
                            <th>
                                Acciones
                            </th>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>
                                        {{ $user->id }}
                                    </td>
                                    <td>
                                        {{ $user->name }}
                                    </td>
                                    <td>
                                        {{ $user->email }}
                                    </td>
                                    <td>
                                        @if($user->role == 'admin')
                                            Administrador
                                        @else
                                            Usuario
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('users.edit', $user->id) }}">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a> | 
                                        @if(Auth::user()->id != $user->id )
                                            <a href="{{ route('users.destroy', $user->id) }}" onclick="return confirm('¿Seguro que desea eliminar este usuario?')">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>
                                        @elseif($user->id == 1)
                                            
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Oops!!</h4>
                <p class="card-category">Usted no tiene permiso para acceder a este módulo</p>
            </div>
        </div>
    @endif
@endsection
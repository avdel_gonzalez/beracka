@extends('auth.layouts.app')

@section('content')
<h2>
    Agregar una nueva norma
</h2>
<div class="container">
    {!! Form::open(['action' => 'VideosController@store', 'method' => 'post']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nombre del video') !!}
        </div>
        <div class="form-group">
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre del video', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Descripción') !!}
        </div>
        <div class="form-group">
            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Url del video', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection
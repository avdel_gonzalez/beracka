@extends('auth.layouts.app')

@section('content')
<h2>
    Editar propiedad: {{ $video->name }}
</h2>
<div class="container">
    {!! Form::open(['action' => ['VideosController@update', $video], 'method' => 'put']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nombre de la norma') !!}
        </div>
        <div class="form-group">
            {!! Form::text('name', $video->name, ['class' => 'form-control', 'placeholder' => 'Nombre de la norma', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Descripción') !!}
        </div>
        <div class="form-group">
            {!! Form::text('description', $video->description, ['class' => 'form-control', 'placeholder' => 'Url de la norma', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Enviar',['class' => 'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
</div>
@endsection
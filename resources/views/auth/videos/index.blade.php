@extends('auth.layouts.app')

@section('content')
        <a href="{{ route('videos.create') }}" class="btn btn-warning">
            Agregar un video
        </a>
        <div class="card card-plain">
            <div class="card-header card-header-warning">
                <h4 class="card-title mt-0">Tutoriales</h4>
                <p class="card-category">Videos para mostrar a los usuarios visitantes</p>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>
                                Id
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Url
                            </th>
                            <th>
                                Acciones
                            </th>
                        </thead>
                        <tbody>
                            @foreach($videos as $video)
                                <tr>
                                    <td>
                                        {{ $video->id }}
                                    </td>
                                    <td>
                                        {{ $video->name }}
                                    </td>
                                    <td>
                                        {{ $video->description }}
                                    </td>
                                    <td>
                                        <a href="{{ route('videos.edit', $video->id) }}">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                        <a href="{{ route('videos.destroy', $video->id) }}" onclick="return confirm('¿Seguro que desea eliminar este usuario?')">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection
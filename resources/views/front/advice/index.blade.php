@extends('welcome')

@section('content')
<!-- start banner Area -->
<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Asesorías en Normas de Habilitación en Salud				
							</h1>	
							<p class="text-white link-nav"><a href="{{ route('index') }}">Inicio</a></p>
						</div>											
					</div>
				</div>
			</section>
            <!-- End banner Area -->	
            
            <!-- Start service Area -->
			<section class="service-area section-gap" id="service">
				<div class="container">					
					<div class="row">
                        @foreach($healt_habilitations as $advisory)
                            <div class="col-lg-4">
                                <div class="detail">
                                    <h4>{{ $advisory->name }}</h4>
                                    <p>
                                        <a href="{{ $advisory->description }}" target="_blank"><strong>Click aquí para ver norma</strong></a><br />
                                    </p>
                                </div>
                            </div>
                        @endforeach										
					</div>
				</div>	
			</section>
			<!-- End service Area -->	
@endsection
@extends('welcome')

@section('content')
<!-- start banner Area -->
<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Proyectos en ejecución				
							</h1>	
							<p class="text-white link-nav"><a href="{{ route('index') }}">Inicio</a></p>
						</div>											
					</div>
				</div>
			</section>
            <!-- End banner Area -->	
            
            <!-- Start service Area -->
			<section class="service-area section-gap" id="service">
				<div class="container">					
					<div class="row">
                        @foreach($projects as $project)
                            <div class="col-lg-4">
                                <div class="single-service">
                                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            @foreach($project->projectImages as $i => $image)
                                                <div class="carousel-item <?php if($i == 0): ?>active<?php endif ?>">
                                                    <img src="{{ asset('img/projects/' . $image->name) }}" class="d-block w-100" style="max-height: 300px">
                                                </div>
                                            @endforeach
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="detail">
                                    <h4>{{ $project->name }}</h4>
                                    <p>
                                        <strong>Habitaciones: </strong>{{ $project->rooms }}<br />
                                        <strong>Tamaño: </strong>{{ $project->size }}<br />
                                        <strong>Impuestos anuales: </strong>{{ $project->annual_taxes }}<br />
                                        <strong>Precio: </strong>{{ $project->price }}<br />
                                    </p>
                                </div>
                            </div>
                        @endforeach										
					</div>
				</div>	
			</section>
			<!-- End service Area -->	
@endsection
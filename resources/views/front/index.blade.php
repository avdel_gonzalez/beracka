@extends('welcome')

@section('content')
<!-- start banner Area -->
<section class="banner-area relative" id="home">
				<div class="overlay overlay-bg"></div>	
				<div class="container">
					<div class="row fullscreen d-flex align-items-center justify-content-start">
						<div class="banner-content col-lg-9">
							<h6 class="text-white">Constructura e inmobiliaria</h6>
							<h1 class="text-white">
								{{ $company->name }}
							</h1>
							<p class="pt-20 pb-20 text-white">
								{{ $company->description }}
							</p>
							<a href="#service" class="primary-btn text-uppercase">Conócenos</a>
						</div>											
					</div>
				</div>					
			</section>
			<!-- End banner Area -->
			<!-- Start service Area -->
			<section class="service-area section-gap" id="service">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-70 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10">Objetivo y enfoque</h1>
								<p>{{ $company->objectives }}</p>
							</div>
						</div>
					</div>						
					<div class="row">
						<div class="col-lg-4">
							<div class="single-service">
								<div class="thumb">
									<img class="img-fluid" src="{{ asset('img/front/s1.jpg') }}" alt="">
								</div>
								<div class="detail">
									<h4>Construcción</h4>
									<p>
									Obras civiles,estudios de suelos, calculo y diseño estructural ,Adecuaciones, Remo delaciones ,Etc.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="single-service">
								<div class="thumb">
									<img class="img-fluid" src="{{ asset('img/front/s2.jpg') }}" alt="">
								</div>
								<div class="detail">
									<h4>Inmobiliaria</h4>
									<p>
									Compra y venta de inmuebles , Avaluos , Administración de inmuebles , Etc.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="single-service">
								<div class="thumb">
									<img class="img-fluid" src="{{ asset('img/front/s3.jpg') }}" alt="">
								</div>
								<div class="detail">
									<h4>Asesorías en normas</h4>
									<p>
									Asesorias en normas urbaisticas y de construcción, consultorias e interventorias de obras , Asesorias en Normas de habilitación en salud,Etc .
									</p>
								</div>
							</div>
						</div>												
					</div>
				</div>	
			</section>
			<!-- End service Area -->	


			<!-- Start feature Area -->
			<section class="feature-area section-gap" id="feature">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="single-feature">
								<h4><span class="lnr lnr-flag"></span>Misión</h4>
								<p>
									{{ $company->mission }}
								</p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="single-feature">
								<h4><span class="lnr lnr-star"></span>Visión</h4>
								<p>
									{{ $company->vission }}
								</p>								
							</div>
						</div>						
					</div>
				</div>	
			</section>
			<!-- End feature Area -->									

			<!-- Start galery Area -->
			<section class="galery-area section-gap" id="gallery">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-70 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10">Nuestro equipo</h1>
							</div>
						</div>
					</div>					
					<div class="row">
						@foreach($members as $member)
							<div class="col-md-4">
								<div class="card border-dark">
									<div class="card-header text-center">
										<strong>{{ $member->name }}</strong>
									</div>
									<div class="card-body">
										@foreach($member->memberImages as $image)
											<img src="{{ asset('img/members/' . $image->name) }}" style="max-width: 300px">
										@endforeach
									</div>
									<div class="card-footer text-center">
										{{ $member->description }}
									</div>
								</div>
							</div>	
						@endforeach																			
					</div>
				</div>	
			</section>
            <!-- End galery Area -->
@endsection
@extends('welcome')

@section('content')
<!-- start banner Area -->
<section class="banner-area relative" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Tutoriales
							</h1>	
							<p class="text-white link-nav"><a href="{{ route('index') }}">Inicio</a></p>
						</div>											
					</div>
				</div>
			</section>
            <!-- End banner Area -->	
            
            <!-- Start service Area -->
			<section class="service-area section-gap" id="service">
				<div class="container">					
					<div class="row">
                        @foreach($videos as $video)
                            <div class="col-lg-4">
                                <div class="detail">
                                    <h4>{{ $video->name }}</h4>
                                    <?php $embed = str_replace('watch?v=', 'embed/', $video->description) ?>
                                    <iframe width="300px" height="250px" src="{{ $embed }}"></iframe>
                                </div>
                            </div>
                        @endforeach										
					</div>
				</div>	
			</section>
			<!-- End service Area -->	
@endsection
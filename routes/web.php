<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('index');
Route::get('/ejecuting-projects', 'PagesController@ejecutingProjects')->name('ejecuting-projects');
Route::get('/ejecuted-projects', 'PagesController@ejecutedProjects')->name('ejecuted-projects');
Route::get('/rental-properties', 'PagesController@rentalProperties')->name('rental-properties');
Route::get('/sale-properties', 'PagesController@saleProperties')->name('sale-properties');
Route::get('/advice', 'PagesController@adviceHealt')->name('advice');
Route::get('/videoTutorials', 'PagesController@videos')->name('videoTutorials');
Route::get('/contact', 'ContactController@index')->name('contact');
Route::post('/send-mail', 'ContactController@contact')->name('send-mail');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){
    Route::resource('users', 'UsersController');
    Route::get('/users/{id}/destroy', [
        'uses' => 'UsersController@destroy',
        'as' => 'users.destroy'
    ]);
    
    Route::resource('company', 'CompanyController');
    
    Route::resource('projects', 'ProjectsController');
    Route::get('/projects/{id}/destroy', [
        'uses' => 'ProjectsController@destroy',
        'as' => 'projects.destroy'
    ]);
    Route::get('/imageproject/{id}/imageDelete', [
        'uses' => 'ProjectsController@imageDelete',
        'as' => 'imageproject.destroy'
    ]);

    Route::resource('properties', 'PropertiesController');
    Route::get('/properties/{id}/destroy', [
        'uses' => 'PropertiesController@destroy',
        'as' => 'properties.destroy'
    ]);
    Route::get('/imageproperty/{id}/imageDelete', [
        'uses' => 'PropertiesController@imageDelete',
        'as' => 'imageproperty.destroy'
    ]);

    Route::resource('members', 'MembersController');
    Route::get('/members/{id}/destroy', [
        'uses' => 'MembersController@destroy',
        'as' => 'members.destroy'
    ]);
    Route::get('/imagemember/{id}/imageDelete', [
        'uses' => 'MembersController@imageDelete',
        'as' => 'imagemember.destroy'
    ]);

    Route::resource('healt_habilitations', 'AdviceController');
    Route::get('/healt_habilitations/{id}/destroy', [
        'uses' => 'AdviceController@destroy',
        'as' => 'healt_habilitations.destroy'
    ]);

    Route::resource('videos', 'VideosController');
    Route::get('/videos/{id}/destroy', [
        'uses' => 'VideosController@destroy',
        'as' => 'videos.destroy'
    ]);
});